try:
    import dns.resolver 
except:
    print("Warning: dnspython is not installed!\nTry: pip3 install dnspython\n")
    exit()

coloredExists = False

try:
    from colored import fg, attr
    coloredExists = True
except:
    print("Warning: colored is not installed!\nTry: pip3 install colored\n")

if coloredExists:
    OKColor = fg('#008000')
    NOTColor = fg('#FF0000')
    MovColor = fg('#FFFF00')
    ResColor = attr('reset')
else:
    OKColor = ""
    NOTColor = ""
    MovColor = ""
    ResColor = ""

print("this action maybe take a few minutes...\n\n")

File = open('site.list','r')
site_list = File.readlines()
site_list = [k.replace("\n", "") for k in site_list]
File.close()

my_resolver = dns.resolver.Resolver()
my_resolver.nameservers = ['8.8.8.8']

for url in site_list:
    answer = my_resolver.query(url)
    ip_list = []
    for rdata in answer:
        ip_list.append(rdata.address)
    if '10.10.34.34' in ip_list or '10.10.34.35' in ip_list or '10.10.34.36' in ip_list:
        status= 'DNS is block'
        print (url + ":" + NOTColor + " NOT!" + ResColor)
    else:
        status= 'No DNS_block detacted'
        print (url + ":" + OKColor + " OK!" + ResColor)
    
    report = open('report.log', 'a')
    report.write('Name: ' + url + '\n')
    report.write('Status: ' + status + '\n')
    report.close()

